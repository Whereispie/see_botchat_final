
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Robot {

    Robot() {
    }

    static boolean quit = false;
    private static BotCommands getCommand;
    private static Random randomGenerator = new Random();
    private static ArrayList<String> botVocabulary = new ArrayList<>();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    public static void main(String[] args) throws IOException {
        BufferedReader bufSource = new BufferedReader(new InputStreamReader(new FileInputStream("src/BotLib.txt"), "windows-1251"));
        while (bufSource.ready()) {
            botVocabulary.add(bufSource.readLine());
        }
        bufSource.close();
        System.out.println("Hello! I like random facts. For HELP please type help");


        while (!quit) {
            int randomNumber = randomGenerator.nextInt(botVocabulary.size());
            String humanQuestion = reader.readLine();
            String botAnswer = botVocabulary.get(randomNumber);
            System.out.println("Ohh human ! Do you know fact № " + botAnswer);

            switch (humanQuestion) {
                case "date":
                    getCommand().date();
                    break;
                case "quit":
                    System.out.println("Bye, bye human !");
                    quit = true;
                    break;
                case "time":
                    getCommand().time();
                    break;
                case "create":
                    System.out.println("Choose filepath to fileActions, you can find your file in /src folder");
                    getCommand().fileActions("create");
                    break;
                case "delete":
                    getCommand().fileActions("delete");
                    break;
                case "help":
                    getCommand().help();
                    break;
                case "sort":
                    System.out.println("Place digits in one line and split with space");
                    getCommand().sort();
                    break;
                case "silent":
                    while (!quit) {
                        System.out.println("I'm sleeping, input [get up]");
//                        reader.readLine();
                        if (reader.readLine().equals("get up"))
                            quit = true;
                    }
                    quit = false;
                    System.out.println("I'm awake");
                    break;
            }
        }
    }

    private static BotCommands getCommand() {
        if (null == getCommand) {
            getCommand = new BotCommands();
        }
        return getCommand;
    }
}
