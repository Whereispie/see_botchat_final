
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class BotCommands extends Robot {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private String fileSeparator = System.getProperty("file.separator");


    void date() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }

    void time() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }

    void fileActions(String option) throws IOException {
        switch (option) {
            case "create":
                String relativePath;
                String userInput = reader.readLine();
                relativePath = "src" + fileSeparator + userInput + ".txt";
                File file = new File(relativePath);
                if (file.createNewFile()) {
                    System.out.println(relativePath + "File was successfully created in /src directory");
                } else {
                    System.out.printf("In current place, file %s already exists", relativePath);
                }
                break;
            case "delete":
                System.out.println("Choose path to delete in src/ location");
                String userInputDel = reader.readLine();
                relativePath = "src" + fileSeparator + userInputDel + ".txt";
                File filePath = new File(relativePath);
                boolean deleted = filePath.delete();
                if (deleted) {
                    System.out.printf("File was %s deleted", filePath);
                } else System.out.printf("There is no %s to deleted", filePath);
                break;
        }
    }

    void help() {
        System.out.println("Bot can interact with following commands:");
        System.out.println("[silent/get up] – bot will sleep/awake");
        System.out.println("[date] – current date output");
        System.out.println("[time] – current time output");
        System.out.println("[create] – create .txt file in /src location");
        System.out.println("[delete] – delete .txt file in /src location");
        System.out.println("[quit] – exit from chat");
    }

    void sort() throws IOException {
        String line = reader.readLine();
        String[] tokens = line.split(" ");
        int[] numbers = new int[tokens.length];
        while (!quit) {
            if (reader.readLine().contains("quit")) {
                break;
            } else {
                for (int i = 0; i < numbers.length; i++)
                    numbers[i] = Integer.parseInt(tokens[i]);
            }
        }  // 5 3


        int temp = 0;
        boolean isSorted = false;

        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i] < numbers[i + 1]) {
                    isSorted = false;

                    temp = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(numbers));
    }
}
